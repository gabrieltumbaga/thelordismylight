import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'

//import Bootstrap from 'react-bootstrap'
//import Button from 'react-bootstrap/lib/Button';

import Header from '../components/header'
import MyModal from '../components/mymodal'
import MySidenav from '../components/mysidenav'
import MyFooter from '../components/footer'

import './bootstrap.css'
import './index.scss'
import './style.scss'
import './responsive.scss'


const jquery = <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" ></script>
const bootstrap = <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
//const Layout = ({ children, data }) => (
class Layout extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false
    };
  }

  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    return (
      <div>
        <Helmet
          title={this.props.data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
          ]}
        >
          <html lang="en" />
          <body className="clearfix" />
        </Helmet>

        <div id="title-holder">
          <div className="main-title">
            <Link to="/" id="brand">The Lord<br />Is My Light</Link>
          </div>
          <div className="clearfix"><div className="sub-title">Photography by Edgar Tumbaga</div></div>
        </div>

        <MySidenav />

        <main role="main" className="container">
          <div className="starter-template">
            <div>
              <button type="button" className="btn btn-primary" onClick={this.handleShow}>Open modal</button>
              <MyModal modalTitle="My Awesome Modal" buttonText="Okay!" buttonAction={this.handleClose} modalShowHide={this.state.show}>
                Modal Body Goes Here
              </MyModal>
              {this.props.children()}
            </div>
          </div>

        </main>
        
        <MyFooter />
      </div>
    );
  }

}

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
