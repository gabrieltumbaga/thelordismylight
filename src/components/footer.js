import React from 'react'
import Link from 'gatsby-link'

const MyFooter = () => (
  <div>
    <footer>
      <div className="inner">
        &copy; Copyright 2018 Edgar Tumbaga
      </div>
    </footer>

  </div>
)

export default MyFooter
