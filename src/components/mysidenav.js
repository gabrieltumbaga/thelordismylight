import React from 'react'
import Link from 'gatsby-link'
import {Navbar, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap'


const MySidenav = (props) => (
  <div>
    <Navbar>
      <Navbar.Header>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/gallery" activeClassName="active">Gallery</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/about" activeClassName="active">About</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/contact" activeClassName="active">Contact</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/credits" activeClassName="active">Credits</Link>
          </li>
        </ul>

      </Navbar.Collapse>
  </Navbar>
  </div>
)

export default MySidenav;
