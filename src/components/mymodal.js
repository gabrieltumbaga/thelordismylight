import React from 'react'
import Link from 'gatsby-link'
import {Modal, Button} from 'react-bootstrap'




const MyModal = ({ children, modalTitle, buttonText, buttonAction, modalShowHide }) => (
  <div>
    <div className="static-modal">
      <Modal show={modalShowHide}>
        <Modal.Header>
          <Modal.Title>{modalTitle}</Modal.Title>
        </Modal.Header>

        <Modal.Body>{children}</Modal.Body>

        <Modal.Footer>
          <Button onClick={buttonAction}>{buttonText}</Button>
        </Modal.Footer>
      </Modal>
    </div>
  </div>
)

export default MyModal
