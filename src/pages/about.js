import React from 'react'
import Link from 'gatsby-link'

const AboutPage = () => (
  <div>
    <h1>About</h1>
    <p>Welcome to the about page</p>
  </div>
)

export default AboutPage
