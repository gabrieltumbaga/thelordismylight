import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>My Page Title</h1>
    <p className="lead">Use this document as a way to quickly start any new project. All you get is this text and a mostly barebones HTML document.</p>
    <p className="">Use this document as a way to quickly start any new project. All you get is this text and a mostly barebones HTML document.</p>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tristique mi lectus, vel tincidunt tellus lacinia sed. Integer nec volutpat dolor. Aliquam sed velit eget
    </p>
    <h4>Sub Heading</h4>
    <p>
      In pellentesque non erat eget posuere. Nulla aliquam, enim vel sagittis posuere, velit dui cursus dolor, sit amet maximus libero risus in sem. Quisque sodales neque s
    </p>
    <p>
      Vivamus sed fermentum enim, et venenatis ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris imperdiet ut urna quis malesuada. Sed in sodales tor
    </p>
    <hr />
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tristique mi lectus, vel tincidunt tellus lacinia sed. Integer nec volutpat dolor. Aliquam sed velit eget
    </p>
    <div className="button-holder">
      <button className="btn btn-default">Button Text</button>
    </div>

  </div>
)

export default IndexPage
