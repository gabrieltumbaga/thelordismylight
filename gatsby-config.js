module.exports = {
  siteMetadata: {
    title: 'The Lord is my Light Photography',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass'
  ],
}
